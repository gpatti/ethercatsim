# EtherCATSim

Simulation model for OMNeT++.
Developed and maintained by Gaetano Patti (gaetano.patti (at) unict.it)

## Introduction to EtherCATSim

EtherCATSim is a simulation model for OMNeT++ version 5.5.1 (<a href="https://omnetpp.org">https://omnetpp.org</a>) that implement the Standard EtherCAT protocol and the Priority-Driven Swapping-Based Scheduling of Aperiodic Real-Time Messages approach presented in <a href="#tii15">[1]</a>.

## Installation instructions
1. Download and install the OMNeT++ framework at https://omnetpp.org (the simulation model has been tested with version 5.5.1)
1. Import this project folder to OMNeT++ workspace
1. Build the project
1. Run the simulation example

## References
[1]<a name="tii15">&nbsp;</a>Lucia Lo Bello, Enrico Bini, Gaetano Patti, "Priority-Driven Swapping-Based Scheduling of Aperiodic Real-Time Messages Over EtherCAT Networks", IEEE Transactions on Industrial Informatics, vol. 11, no. 3, pp. 741-751, Aug. 2014.
