// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TrafficGen.h"
#include "../datalink/EtherCATPackets_m.h"

Define_Module(TrafficGen);

void TrafficGen::initialize() {
    samplingPeriod = par("samplingPeriod").doubleValue();
    asyncExpMean = par("asyncExpMean").doubleValue();
    deadline = par("deadline"); //expressed in us
    startTime = par("startTime").doubleValue();
    stopTime = par("stopTime").doubleValue();
    randomDeadline = par("randomDeadline").boolValue();

    started = false;
    fprd_index = 0;
    apdu_index = 0;

    cMessage *msg = new cMessage();
    msg->setKind(TIMER_START);
    scheduleAt(startTime, msg);

    if(stopTime > 0) {
        msg = new cMessage();
        msg->setKind(TIMER_STOP);
        scheduleAt(stopTime, msg);
    }
}

void TrafficGen::handleMessage(cMessage *msg) {
    if(msg->isSelfMessage()) {
        switch(msg->getKind()) {
            case TIMER_START: {
                    started = true;
                    sendFPRD();
                    cMessage *fpmsg = msg->dup();
                    fpmsg->setKind(TIMER_FPRD);
                    scheduleAt(simTime()+samplingPeriod, fpmsg->dup());
                    fpmsg->setKind(TIMER_APDU);
                    scheduleAt(simTime()+exponential(asyncExpMean), fpmsg);
                }
                break;
            case TIMER_STOP:
                started = false;
                break;
            case TIMER_FPRD:
                if(started) {
                    scheduleAt(simTime()+samplingPeriod, msg->dup());
                    sendFPRD();
                }
                break;
            case TIMER_APDU:
                if(started) {
                    scheduleAt(simTime()+exponential(asyncExpMean), msg->dup());
                    sendAPDU();
                }
                break;
        }
        delete msg;
        return;
    }
}

void TrafficGen::sendFPRD() {
    FPRDData *fprd = new FPRDData();
    fprd->setTxtime(simTime());
    fprd->setPktid(fprd_index);
    fprd_index++;
    fprd->setKind(0x04);
    send(fprd, "lowerLayerOut");
}

void TrafficGen::sendAPDU() {
    APDUHeader *apdu = new APDUHeader();
    long int tdl = 0;
    if(randomDeadline) tdl = ((long int)(simTime().dbl()*1e6)+getRandomDeadline());
    else tdl = ((long int)(simTime().dbl()*1e6)+deadline);
    apdu->setDL(tdl);
    apdu->setTxtime(simTime());
    apdu->setPktnum(apdu_index);
    apdu->setKind(0x0F);
    apdu_index++;
    send(apdu, "lowerLayerOut");
}

int TrafficGen::getRandomDeadline() {
    int dd = 500+(100*intuniform(0,2));

    simsignal_t sig = registerSignal("DeadlineDist");
    emit(sig,dd);

    return dd;
}
