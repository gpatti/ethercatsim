// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherCATSlaveDatalink.h"

Define_Module(EtherCATSlaveDatalink);

void EtherCATSlaveDatalink::initialize() {
    latency = par("processingLatency").doubleValue();

    address = -1;
    txpdata = -1;
    psended = false;
    asended = false;
    state = STATE_IDLE;
    rxcounter = 0;
    bufferT12PDU = nullptr;
    out_apdu = nullptr;
    in_apdu = nullptr;
    fprd_data_buff = nullptr;
}

void EtherCATSlaveDatalink::handleMessage(cMessage *msg) {
    if(msg->getArrivalGate() == gate("upperLayerIn")) {
        if(msg->getKind() == 0x0F) {//APDU
            APDUHeader *apdu = dynamic_cast<APDUHeader *>(msg);
            asyncQueue.enqueue(apdu);

            simsignal_t sigGenAPDU = registerSignal("GenAPDU");
            emit(sigGenAPDU, address);
            simsignal_t sigQueue = registerSignal("QueueLength");
            emit(sigQueue, asyncQueue.getSize());

        } else if(msg->getKind() == 0x04) { //FPRD
            if(fprd_data_buff !=nullptr) {
                delete fprd_data_buff;
                fprd_data_buff = nullptr;
            }
            fprd_data_buff = dynamic_cast<FPRDData *>(msg);

            simsignal_t sigGenFPRD = registerSignal("GenFPRD");
            emit(sigGenFPRD, address);
        }
        return;
    }

    if(!msg->isPacket()) {
        ConfigMsg *cmsg = dynamic_cast<ConfigMsg *>(msg);
        address = cmsg->getCurAddr()+1;
        cmsg->setCurAddr(address);
        send(cmsg->dup(), "lowerLayerOut");
        delete cmsg;
        state = STATE_ETH_HDR_WAIT;
        return;
    }

    switch(state) {
        case STATE_IDLE:
            EV << getFullName() << " [" << simTime().dbl() << "]: Unexpected Packet!" << endl;
            delete msg;
            return;
        case STATE_ETH_HDR_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Ethernet header..." << endl;
            state = STATE_ETH_HDR;
            rxcounter = 21;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_ETH_HDR:
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_ETC_HDR_WAIT;
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_ETC_HDR_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving EtherCAT header..." << endl;
            state = STATE_ETC_HDR;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_ETC_HDR:
            state = STATE_T12PDU_HDR_WAIT;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_T12PDU_HDR_WAIT: {
                bufferT12PDU = dynamic_cast<Type12PDUHeader *>(msg->dup());
                if(bufferT12PDU == nullptr) {
                    error("Error packet type");
                }
                EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Type 12 PDU header..." << endl;
                state = STATE_T12PDU_HDR;
                rxcounter = 9;
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_T12PDU_HDR:
            rxcounter--;
            if(rxcounter == 0) {
                if(bufferT12PDU->getCMD() == 0x04) {
                    state = STATE_FPRD_DATA_WAIT;
                } else if(bufferT12PDU->getCMD() == 0x0F) {
                    state = STATE_APDU_HDR_WAIT;
                }
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_FPRD_DATA_WAIT: {
                Type12PDUHeaderFPRD *fprd_hdr = dynamic_cast<Type12PDUHeaderFPRD *>(bufferT12PDU);
                psended = false;
                if(fprd_hdr->getADP() == address) {
                    FPRDData *data = new FPRDData();
                    if(fprd_data_buff != nullptr) {
                        EV << getFullName() << " [" << simTime().dbl() << "]: Sending FPRD..." << endl;
                        data->setTxtime(fprd_data_buff->getTxtime());
                        data->setHasData(true);
                        psended = true;
                        delete fprd_data_buff;
                        fprd_data_buff = nullptr;
                    } else {
                        EV << getFullName() << " [" << simTime().dbl() << "]: Sending FPRD (nothing to transmit)..." << endl;
                        data->setTxtime(-1);
                        data->setHasData(false);
                    }
                    sendDelayed(data, latency, "lowerLayerOut");
                    delete msg;
                } else {
                    sendDelayed(msg, latency, "lowerLayerOut");
                }
                rxcounter = fprd_hdr->getLEN()-1;
                if(rxcounter == 0)
                    state = STATE_T12PDU_WKC_WAIT;
                else
                    state = STATE_DATA;
            }
            return;
        case STATE_DATA:
            rxcounter--;
            if(rxcounter==0) {
                EV << getFullName() << " [" << simTime().dbl() << "]: End Data! Waiting for WCK..." << endl;
                state = STATE_T12PDU_WKC_WAIT;
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_T12PDU_WKC_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving WKC..." << endl;
            if(psended || asended) {
                Type12WKC *wkc = dynamic_cast<Type12WKC *>(msg);
                wkc->setWkc(wkc->getWkc()+1);
            }
            state = STATE_T12PDU_WKC;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_T12PDU_WKC:
            if(bufferT12PDU->getNEXT()) {
                state = STATE_T12PDU_HDR_WAIT;
            } else {
                state = STATE_ETH_FCS_WAIT;
            }
            delete bufferT12PDU;
            bufferT12PDU = nullptr;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_APDU_HDR_WAIT:
            in_apdu = dynamic_cast<APDUHeader *>(msg);
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving APDU! with deadline: " << in_apdu->getDL() << endl;
            asended = false;
            if(out_apdu == nullptr) out_apdu = asyncQueue.dequeue();
            if(out_apdu != nullptr && (out_apdu->getDL() < in_apdu->getDL())) {
                EV << getFullName() << " [" << simTime().dbl() << "]: Do Swapping..." << endl;
                out_apdu->setADP(address);
                sendDelayed(out_apdu, latency, "lowerLayerOut");
                out_apdu = nullptr;
                if(in_apdu->getDL() < 0xFFFFFFFFFFF0) {
                    asyncQueue.enqueue(in_apdu);
                } else {
                    delete in_apdu;
                }
                in_apdu = nullptr;
                asended = true;
                simsignal_t sigQueue = registerSignal("QueueLength");
                emit(sigQueue, asyncQueue.getSize());
            } else {
                EV << getFullName() << " [" << simTime().dbl() << "]: Forwarding APDU..." << endl;
                sendDelayed(in_apdu, latency, "lowerLayerOut");
                if(out_apdu != nullptr)
                    asyncQueue.enqueue(out_apdu);
                out_apdu = nullptr;
                in_apdu = nullptr;
            }
            rxcounter = 11;
            state = STATE_APDU_HDR;
            return;
        case STATE_APDU_HDR:
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_APDU_DATA_WAIT;
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_APDU_DATA_WAIT:
            rxcounter = bufferT12PDU->getLEN()-13;
            if(rxcounter == 0)
                state = STATE_T12PDU_WKC_WAIT;
            else
                state = STATE_DATA;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_ETH_FCS_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Ethernet FCS..." << endl;
            rxcounter = 3;
            state = STATE_ETH_FCS;
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
        case STATE_ETH_FCS:
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_ETH_HDR_WAIT;
            }
            sendDelayed(msg, latency, "lowerLayerOut");
            return;
    }
}
