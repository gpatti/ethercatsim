// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef DATALINK_ORDEREDQUEUE_H_
#define DATALINK_ORDEREDQUEUE_H_

#include <omnetpp.h>
#include "EtherCATPackets_m.h"
#include <queue>

using namespace std;

class CompareAPDU {
public:
    bool operator()(APDUHeader *apdu1, APDUHeader *apdu2);
};

class OrderedQueue {
protected:
    priority_queue<APDUHeader *, vector<APDUHeader *>, CompareAPDU> queue;

public:
    OrderedQueue();
    virtual ~OrderedQueue();
    virtual void enqueue(APDUHeader *apdu);
    virtual APDUHeader *dequeue();
    virtual int getSize();
};

#endif /* DATALINK_ORDEREDQUEUE_H_ */
