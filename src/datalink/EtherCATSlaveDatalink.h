// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __ETHERCATSIM_ETHERCATSLAVEDATALINK_H_
#define __ETHERCATSIM_ETHERCATSLAVEDATALINK_H_

#include <omnetpp.h>
#include "EtherCATPackets_m.h"
#include "OrderedQueue.h"

using namespace omnetpp;

class EtherCATSlaveDatalink : public cSimpleModule {
  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    int address;
    OrderedQueue asyncQueue;
    FPRDData *fprd_data_buff;
    double txpdata;
    double latency;

    bool psended;
    bool asended;

    int state;
    int rxcounter;
    Type12PDUHeader *bufferT12PDU;
    APDUHeader *out_apdu;
    APDUHeader *in_apdu;

    enum States {
        STATE_IDLE,
        STATE_ETH_HDR_WAIT,
        STATE_ETH_HDR,
        STATE_ETC_HDR_WAIT,
        STATE_ETC_HDR,
        STATE_T12PDU_HDR_WAIT,
        STATE_T12PDU_HDR,
        STATE_FPRD_DATA_WAIT,
        STATE_DATA,
        STATE_APDU_HDR_WAIT,
        STATE_APDU_HDR,
        STATE_APDU_DATA_WAIT,
        STATE_T12PDU_WKC_WAIT,
        STATE_T12PDU_WKC,
        STATE_ETH_FCS_WAIT,
        STATE_ETH_FCS,
        STATE_LCLR_DATA_WAIT
    };
};

#endif
