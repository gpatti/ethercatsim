// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "OrderedQueue.h"

bool CompareAPDU::operator ()(APDUHeader *apdu1, APDUHeader *apdu2) {
    if(apdu1->getDL() < apdu2->getDL()) return true;
    return false;
}

void OrderedQueue::enqueue(APDUHeader *apdu) {
    queue.push(apdu);
}

APDUHeader *OrderedQueue::dequeue() {
    if(queue.empty()) return NULL;
    APDUHeader *ah = queue.top()->dup();
    queue.pop();
    return ah;
}

int OrderedQueue::getSize() {
    return queue.size();
}

OrderedQueue::OrderedQueue() {

}

OrderedQueue::~OrderedQueue() {

}
