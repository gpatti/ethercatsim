// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherCATMaster.h"

Define_Module(EtherCATMaster);

void EtherCATMaster::initialize() {
    datarate = par("datarate");
    ifg = par("interFrameGap");
    numAT12PDU = par("numAT12PDU");
    numFPRD = par("numFPRD");
    sizeAT12PDU = par("sizeAT12PDU");
    sizeFPRD = par("sizeFPRD");
    byteTime = (double)8/(double)datarate;

    state = STATE_ETH_HDR_WAIT;

    deadlinehit=0;
    deadlinemiss=0;
    nodeaddr= new int[numAT12PDU];
    deadlines =  new double[numAT12PDU];
    delays = new double[numAT12PDU];
    indexAT12PDU = 0;
    FPRDdelay = -1;
    lastFrameStart = 0;
    bufferT12PDU = NULL;

    if(par("autoaddr").boolValue()) {
        ConfigMsg *cmsg = new ConfigMsg();
        cmsg->setCurAddr(0);
        send(cmsg, "lowerLayerOut");
    } else {
        sendFrame();
    }
}


void EtherCATMaster::sendFrame() {
    int len;
    int byteno = 1;

    //Sending Ethernet Header
    EthernetHeader *ethhdr = new EthernetHeader();
    len = ethhdr->getByteLength()-1;
    send(ethhdr, "lowerLayerOut");
    byteno++;
    for(int i=0; i<len; i++) {
        sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
        byteno++;
    }

    //Sending Ethercat Header
    EthercatHeader *etchdr = new EthercatHeader();
    len = etchdr->getByteLength()-1;
    sendDelayed(etchdr, (byteno*byteTime), "lowerLayerOut");
    byteno++;
    for(int i=0; i<len; i++) {
        sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
        byteno++;
    }

    //Sending Periodic Frames
    int numtslave = 0;
    for(int j=0; j<numFPRD; j++) {
        //FPRD Header
        Type12PDUHeaderFPRD *fprd = new Type12PDUHeaderFPRD();
        if((j-(numtslave*numFPRD)) > numFPRD) numtslave++;
        fprd->setADP((j+1)-(numtslave*numFPRD));
        fprd->setLEN(sizeFPRD);
        if(j==(numFPRD-1) && numAT12PDU==0) fprd->setNEXT(false);
        len = fprd->getByteLength()-1;
        sendDelayed(fprd, (byteno*byteTime), "lowerLayerOut");
        byteno++;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }

        //FPRD payload
        len = sizeFPRD;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }

        //WKC
        Type12WKC *wkc = new Type12WKC();
        len = wkc->getByteLength()-1;
        wkc->setWkc(0);
        sendDelayed(wkc, (byteno*byteTime), "lowerLayerOut");
        byteno++;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }
    }

    //Sending Async Type 12 PDU
    for(int j=0; j<numAT12PDU; j++) {
        //AT12PDU Header
        Type12PDUHeader *at12 = NULL;
        at12 = new Type12PDUHeaderAPDU();
        at12->setLEN(sizeAT12PDU);
        if(j==(numAT12PDU-1)) at12->setNEXT(false);
        len = at12->getByteLength()-1;
        sendDelayed(at12, (byteno*byteTime), "lowerLayerOut");
        byteno++;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }

        //send APDU header
        APDUHeader *apduh = new APDUHeader();
        apduh->setLEN(sizeAT12PDU-(apduh->getByteLength()));
        apduh->setDL(0xFFFFFFFFFFFF);
        apduh->setADP(0xFFFFFFFF);
        len = apduh->getByteLength()-1;
        sendDelayed(apduh, (byteno*byteTime), "lowerLayerOut");
        byteno++;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }

        //at12 payload
        len = sizeAT12PDU-(apduh->getByteLength());
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }

        //WKC
        Type12WKC *wkc = new Type12WKC();
        len = wkc->getByteLength()-1;
        wkc->setWkc(0);
        sendDelayed(wkc, (byteno*byteTime), "lowerLayerOut");
        byteno++;
        for(int i=0; i<len; i++) {
            sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
            byteno++;
        }
    }

    //Sending Ethercat FCS
    EthernetFCS *fcs = new EthernetFCS();
    len = fcs->getByteLength()-1;
    sendDelayed(fcs, (byteno*byteTime), "lowerLayerOut");
    byteno++;
    for(int i=0; i<len; i++) {
        sendDelayed(new BytePacket(), (byteno*byteTime), "lowerLayerOut");
        byteno++;
    }
}

void EtherCATMaster::handleMessage(cMessage *msg) {
    if(msg->isSelfMessage()) {
        switch(msg->getKind()) {
            case TIMER_IFG: {
                delete msg;
                simsignal_t tc = registerSignal("CycleTime");
                emit(tc, simTime().dbl()-lastFrameStart);
                lastFrameStart = simTime().dbl();
                }
                sendFrame();
                return;
            default:
                delete msg;
                return;
        }
    }

    if(!msg->isPacket()) {
        ConfigMsg *cmsg = dynamic_cast<ConfigMsg *>(msg);
        nodeNum = cmsg->getCurAddr();
        EV << getFullName() << " [" << simTime().dbl() << "]: End of configuration! Number of Nodes: " << nodeNum << endl;
        delete msg;
        sendFrame();
        return;
    }

    switch(state) {
        case STATE_IDLE:
            EV << getFullName() << " [" << simTime().dbl() << "]: Unexpected Packet!" << endl;
            delete msg;
            return;
        case STATE_ETH_HDR_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Ethernet header..." << endl;
            state = STATE_ETH_HDR;
            rxcounter = 21;
            delete msg;
            return;
        case STATE_ETH_HDR:
            delete msg;
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_ETC_HDR_WAIT;
            }
            return;
        case STATE_ETC_HDR_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving EtherCAT header..." << endl;
            state = STATE_ETC_HDR;
            delete msg;
            return;
        case STATE_ETC_HDR:
            delete msg;
            state = STATE_T12PDU_HDR_WAIT;
            return;
        case STATE_T12PDU_HDR_WAIT: {
                bufferT12PDU = dynamic_cast<Type12PDUHeader *>(msg);
                EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Type 12 PDU header..." << endl;
                state = STATE_T12PDU_HDR;
                rxcounter = 9;
            }
            return;
        case STATE_T12PDU_HDR:
            delete msg;
            rxcounter--;
            if(rxcounter == 0) {
                if(bufferT12PDU->getCMD() == 0x04) {
                    state = STATE_FPRD_DATA_WAIT;
                } else if(bufferT12PDU->getCMD() == 0x0F) {
                    state = STATE_APDU_HDR_WAIT;
                }
            }
            return;
        case STATE_FPRD_DATA_WAIT: {
                Type12PDUHeaderFPRD *fprd_hdr = dynamic_cast<Type12PDUHeaderFPRD *>(bufferT12PDU);
                EV << getFullName() << " [" << simTime().dbl() << "]: Receiving FPRD data From: " << fprd_hdr->getADP() << endl;
                rxcounter = fprd_hdr->getLEN()-1;
                FPRDData *data = dynamic_cast<FPRDData *>(msg);
                if(data != NULL) {
                    FPRDdelay = data->getTxtime().dbl();
                }
                if(rxcounter == 0)
                    state = STATE_T12PDU_WKC_WAIT;
                else
                    state = STATE_DATA;
            }
            delete msg;
            return;
        case STATE_DATA:
            delete msg;
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_T12PDU_WKC_WAIT;
            }
            return;
        case STATE_T12PDU_WKC_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving WKC..." << endl;
            delete msg;

            if(FPRDdelay >= 0){
                simsignal_t eeFPRDDelay = registerSignal("E2EFPRDDelay");
                emit(eeFPRDDelay, simTime().dbl()-FPRDdelay);
                FPRDdelay = -1;
            }
            state = STATE_T12PDU_WKC;
            return;
        case STATE_T12PDU_WKC:
            delete msg;
            if(bufferT12PDU->getNEXT() == true) {
                state = STATE_T12PDU_HDR_WAIT;
            } else {
                state = STATE_ETH_FCS_WAIT;
            }
            delete bufferT12PDU;
            bufferT12PDU = NULL;
            return;
        case STATE_APDU_HDR_WAIT: {
                APDUHeader *apdu_hdr = dynamic_cast<APDUHeader *>(msg);
                EV << getFullName() << " [" << simTime().dbl() << "]: Receiving APDU From: " << apdu_hdr->getADP()
                        << " Deadline: " << apdu_hdr->getDL() << endl;
                if(indexAT12PDU >= numAT12PDU)
                    indexAT12PDU = 0;
                deadlines[indexAT12PDU] = ((double)(apdu_hdr->getDL()/1e6));
                delays[indexAT12PDU] = apdu_hdr->getTxtime().dbl();
                nodeaddr[indexAT12PDU] = apdu_hdr->getADP();
                indexAT12PDU++;

                rxcounter = 11;
                state = STATE_APDU_HDR;
            }
            delete msg;
            return;
        case STATE_APDU_HDR:
            delete msg;
            rxcounter--;
            if(rxcounter==0) {
                state = STATE_APDU_DATA_WAIT;
            }
            return;
        case STATE_APDU_DATA_WAIT:
            delete msg;
            rxcounter = bufferT12PDU->getLEN()-13;
            if(rxcounter == 0)
                state = STATE_T12PDU_WKC_WAIT;
            else
                state = STATE_DATA;
            return;
        case STATE_ETH_FCS_WAIT:
            EV << getFullName() << " [" << simTime().dbl() << "]: Receiving Ethernet FCS..." << endl;
            delete msg;
            rxcounter = 3;
            state = STATE_ETH_FCS;
            return;
        case STATE_ETH_FCS:
            delete msg;
            rxcounter--;
            if(rxcounter==0) {

                simsignal_t eeDelay = registerSignal("E2EAPDUDelay");
                simsignal_t sig = registerSignal("NodeDeadlineMiss");
                simsignal_t sig1 = registerSignal("NodeDeadlineHit");

                for(int j=0; j<indexAT12PDU; j++)
                    emit(eeDelay, simTime().dbl()-delays[j]);

                for(int j=0; j<indexAT12PDU; j++) {
                    if(deadlines[j] < 0) break;
                    if((double)(deadlines[j]) < simTime().dbl()) {
                        deadlinemiss++;
                        emit(sig, nodeaddr[j]);
                    } else {
                        deadlinehit++;
                        emit(sig1, nodeaddr[j]);
                    }
                }

                for(int j=0; j<numAT12PDU; j++) {
                    deadlines[j] = -1;
                    delays[j] = -1;
                    nodeaddr[j] = -1;
                }
                indexAT12PDU = 0;

                state = STATE_ETH_HDR_WAIT;
                cMessage *msg = new cMessage();
                msg->setKind(TIMER_IFG);
                scheduleAt(simTime().dbl()+ifg, msg);
            }
            return;
    }
}

void EtherCATMaster::finish() {
    simsignal_t sig = registerSignal("DeadlineMiss");
    emit(sig, deadlinemiss);
    sig = registerSignal("DeadlineHit");
    emit(sig, deadlinehit);
    sig = registerSignal("DeadlineMissRatio");
    emit(sig, (double)((double)deadlinemiss/(double)(deadlinemiss+deadlinehit)));
}
