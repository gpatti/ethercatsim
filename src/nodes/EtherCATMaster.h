// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __ETHERCATSIM_ETHERCATMASTER_H_
#define __ETHERCATSIM_ETHERCATMASTER_H_

#include <omnetpp.h>
#include "../datalink/EtherCATPackets_m.h"

using namespace omnetpp;

class EtherCATMaster : public cSimpleModule {
  protected:
    simtime_t ifg;
    int numAT12PDU;
    int sizeAT12PDU;
    int numFPRD;
    int sizeFPRD;
    long datarate;
    double byteTime;

    int nodeNum;
    int state;

    //counters
    int rxcounter;

    //buffers
    Type12PDUHeader *bufferT12PDU;

    enum Timers {
        TIMER_IFG = 1,
        TIMER_SEND = 0
    };

    //for statistic
    double *deadlines;
    int *nodeaddr;
    double *delays;
    int indexAT12PDU;
    double lastFrameStart;

    double FPRDdelay;

    int deadlinemiss;
    int deadlinehit;

    enum States {
        STATE_IDLE,
        STATE_ETH_HDR_WAIT,
        STATE_ETH_HDR,
        STATE_ETC_HDR_WAIT,
        STATE_ETC_HDR,
        STATE_T12PDU_HDR_WAIT,
        STATE_T12PDU_HDR,
        STATE_FPRD_DATA_WAIT,
        STATE_DATA,
        STATE_APDU_HDR_WAIT,
        STATE_APDU_HDR,
        STATE_APDU_DATA_WAIT,
        STATE_T12PDU_WKC_WAIT,
        STATE_T12PDU_WKC,
        STATE_ETH_FCS_WAIT,
        STATE_ETH_FCS
    };

    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    virtual void sendFrame();
    virtual void finish();
};

#endif
