// Copyright 2014, 2019 Gaetano Patti {gaetano.patti@unict.it}

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherCATPhy.h"

Define_Module(EtherCATPhy);

void EtherCATPhy::handleMessage(cMessage *msg) {
    if(msg->getArrivalGate() == gate("Port1In")) {
        send(msg, "Port0Out");
    } else if(msg->getArrivalGate() == gate("Port0In")) {
        send(msg, "upperLayerOut");
    } else if(msg->getArrivalGate() == gate("upperLayerIn")) {
        if(gate("Port1Out")->getNextGate()->isConnected())
            send(msg, "Port1Out");
        else
            send(msg, "Port0Out");
    }
}
